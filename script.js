"use strict"

const urlUsers = "https://ajax.test-danit.com/api/json/users"
const urlPosts = "https://ajax.test-danit.com/api/json/posts"

class Request {
   get(url) {
      return fetch(url).then((response) => response.json())
         .catch((error) => console.log(error))
   }
   deletePost(postId) {
      return fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
         method: 'DELETE',
      }).catch((error) => console.log(error))
   }
}

class Card {
   render(userObj, postObj) {
      try {
         const cardContainer = document.createElement('div');
         postObj.forEach(({ id, userId, title, body }) => {

            const user = userObj.find(({ id }) => id === userId);

            const { name, surname, email } = user;

            const cardDiv = document.createElement('div');
            cardDiv.setAttribute('class', 'wrapper')
            const cardTitle = document.createElement('p');
            cardTitle.style.textTransform = 'uppercase'
            const cardText = document.createElement('p');
            const cardName = document.createElement('p');
            const cardSurname = document.createElement('p');
            const cardEmail = document.createElement('p');
            const deletePostBtn = document.createElement('button');
            deletePostBtn.setAttribute('id', `${id}`)

            cardTitle.textContent = title;
            cardText.textContent = body;
            cardName.textContent = name;
            cardSurname.textContent = surname;
            cardEmail.textContent = email;
            cardEmail.style.color = 'blue';
            deletePostBtn.textContent = 'Delete Post'

            cardDiv.append(cardTitle, cardText, cardName, cardSurname, cardEmail, deletePostBtn);
            cardContainer.appendChild(cardDiv);
         });
         return cardContainer;
      } catch (err) {
         alert(err)
      }
   }
}

const usersRequest = new Request()
const postsRequest = new Request();
const card = new Card();

Promise.all([usersRequest.get(urlUsers), postsRequest.get(urlPosts)])
   .then(([usersData, postsData]) => {
      const cardContainer = card.render(usersData, postsData);
      document.body.appendChild(cardContainer);
      cardContainer.addEventListener('click', deletePostHandler);
   })
   .catch((error) => {
      console.error(error);
   });

const deletePostHandler = (event) => {
   const postId = event.target.id;
   const delRequest = new Request()
   delRequest.deletePost(postId)
      .then((response) => {
         if (response.status === 200) {
            const cardDiv = event.target.closest('.wrapper');
            cardDiv.remove();
         }
      })
      .catch((error) => {
         console.error(error);
      });
};


